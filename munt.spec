%global ver 2.7.0
%global ver_dashed %(echo %{ver} | sed 's/\\./_/g')

Name:           munt
Version:        %{ver}
Release:        1%{?dist}
Summary:        A software synthesiser emulating e.g. the Roland MT-32

License:        GPL
URL:            https://github.com/%{name}/%{name}
Source0:        https://github.com/%{name}/%{name}/archive/%{name}_%{ver_dashed}.tar.gz

BuildRequires:  cmake
BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  alsa-lib-devel
BuildRequires:  portaudio-devel
BuildRequires:  pulseaudio-libs-devel
BuildRequires:  qt5-qtbase-devel
BuildRequires:  jack-audio-connection-kit-devel
BuildRequires:  desktop-file-utils
#Requires:

%description
A multi-platform software synthesiser emulating (currently inaccurately) pre-GM MIDI devices such as the Roland MT-32, CM-32L, CM-64 and LAPC-I.

%package devel
Summary: Development files for %{name}
Requires: %{name} = %{version}-%{release}

%description devel
Development files for %{name}

%prep
%autosetup -n %{name}-%{name}_%{ver_dashed}


%build
%{cmake} -DCMAKE_BUILD_TYPE:STRING=Release -DBUILD_SHARED_LIBS:BOOL=OFF .
%{cmake_build}


%install
%{cmake_install}
desktop-file-validate %{buildroot}/%{_datadir}/applications/mt32emu-qt.desktop
# Remove libmt32emu docs and don't package them since they clash with fedora's mt32emu package
rm -rf %{buildroot}%{_datadir}/doc/munt/libmt32emu


%files
%license mt32emu/COPYING.txt
%{_datadir}/doc/munt/mt32emu-qt/*
%{_datadir}/doc/munt/smf2wav/*
%{_datadir}/applications/*.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}*.*
%{_libdir}/libmt32emu.a
%{_bindir}/mt32emu-qt
%{_bindir}/mt32emu-smf2wav

%files devel
%{_includedir}/mt32emu.h
%{_includedir}/mt32emu/Types.h
%{_includedir}/mt32emu/config.h
%{_includedir}/mt32emu/SampleRateConverter.h
%{_includedir}/mt32emu/mt32emu.h
%{_includedir}/mt32emu/Synth.h
%{_includedir}/mt32emu/ROMInfo.h
%{_includedir}/mt32emu/globals.h
%{_includedir}/mt32emu/File.h
%{_includedir}/mt32emu/MidiStreamParser.h
%{_includedir}/mt32emu/Enumerations.h
%{_includedir}/mt32emu/FileStream.h
%{_includedir}/mt32emu/c_interface/c_interface.h
%{_includedir}/mt32emu/c_interface/c_types.h
%{_includedir}/mt32emu/c_interface/cpp_interface.h
%{_libdir}/cmake/MT32Emu/
%{_libdir}/pkgconfig/mt32emu.pc

%changelog
* Wed Apr 17 2024 spike <spike@fedoraproject.org> 2.7.0-1
- Updated to 2.7.0 release

* Wed Nov 10 2021 spike <spike@fedoraproject.org> 2.5.0-1
- Updated to 2.5.0 release

* Wed Feb 24 2021 spike <spike@fedoraproject.org> 2.4.0-1
- Updated to 2.4.0 release

* Tue Sep 3 2019 spike <spike@fedoraproject.org> 2.3.0-2.20190903git1ea0469
- Updated to latest git commit

* Sat May 12 2018 spike <spike@fedoraproject.org> 2.3.0-1
- Initial package
